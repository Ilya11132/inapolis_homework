package Credit;

import java.util.Scanner;

public class Credit {

    private String name;
    private int age;
    private int creditAmount;
    private Scanner scanner;

    private String message = null;


    public static void main(String[] args) {
        new Credit().giveCredit();
    }


    public void giveCredit() {
        scanner = new Scanner(System.in);

        System.out.println("Введите ваше имя...");
        name = scanner.nextLine();
        if (name.toLowerCase().equals("bob")) {
            message = "Простите, Бобу сказали не давать, прощайте";
            System.out.println(message);
            return;
        }




        System.out.println("Введите ваш возраст...");
        age = scanner.nextInt();
        if (age < 18) {
            message = "Простите, вы слишком молоды, прощайте";
            System.out.println(message);
            return;
        }


        System.out.println("Введите желаемый размер кредита...");
        creditAmount = scanner.nextInt();
        message = (creditAmount > age * 100) ? "Вы запросили слишком много, предлагаем максимум " + age * 100 :
                "Кредит выдан в размере " + creditAmount;
        System.out.println(message);
    }



  /*   Кредит выдается если:
    Имя не "Bob"
    Возраст 18 лет и больше
    Сумма запрашиваемого кредита не больше возраста умноженного на 100 (для 25 летнего предельная сумма кредита = 2500)
    В других случаях кредит не выдается
    Выдача  кредита -  это вывод в консоль сообщения, что кредит выдан.
    ДЗ закачать в gitlab в свой репозиторий! Сдача ДЗ в формате - ссылка на ваш репозиторий наставнику*/


}
